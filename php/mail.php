<?php

	function form_chars($p1) {
		return nl2br(htmlspecialchars(trim($p1), ENT_QUOTES), false);
	}

	define('FIELDS', array(
		array('name', 'Name'),
		array('email', 'E-mail'),
		array('message', 'Message')
	));

	foreach (FIELDS as $value) {
		$_POST[$value[0]] = form_chars($_POST[$value[0]]);
	}

	$to= "maksym.haltsev@gmail.com";
	$from = "mail@web-agency.com";
	$subject = "Web Agency";

	$headers = "From:".$from."\r\n"
					 . "Reply-To: ".$to."" . "\r\n"
					 . "Content-Type: text/html; charset=utf-8\r\n";
	$body = "<table>";
	foreach (FIELDS as $value) {
		$body .= ($_POST[$value[0]]?"<tr><td>".$value[1].":</td><td>".$_POST[$value[0]]."</td></tr>":"");
	}
	$body .= "</table>";

	if (mail($to, $subject, $body, $headers)) {
		echo 'success';
	}
	else {
		echo 'error';
	}
?>
